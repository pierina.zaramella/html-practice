function loadDoc() {
  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
		
      blogs = JSON.parse(this.responseText);
      html = "";
	  for(i=0;i<10;i++){
		  html += '<div class="c3-panel c3-card"><p id="title">Título: '+blogs[i].title+'</p><p id="description">Descripción: '+ blogs[i].body +'</p></div>';
	  }
	  
		document.getElementById("container-cards").innerHTML = html;     
    }
  };
  xhttp.open("GET", "https://jsonplaceholder.typicode.com/posts", true);
  xhttp.send(); 
}

loadDoc();